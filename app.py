#!env/bin/python3
#
# Copyright (C) Ivan V. Protchenko
#
from os import abort
from flask import Flask, request
import json
app = Flask(__name__)
###########################################################################
js=[{"name":"VLX","description":"Velas"},{"name":"RUB","description":"Russian Ruble"},{"name":"BTC","description":"Bitcoin"},{"name":"USD","description":"US Dollar"},{"name":"LTC","description":"Litecoin"},{"name":"EUR","description":"Euro"},{"name":"DOGE","description":"Dogecoin"},{"name":"DASH","description":"Dash"},{"name":"ETH","description":"Ethereum"},{"name":"UAH","description":"Ukrainian hryvnia"},{"name":"WAVES","description":"Waves"},{"name":"ZEC","description":"Zcash"},{"name":"USDT","description":"Tether"},{"name":"XMR","description":"Monero"},{"name":"XRP","description":"Ripple"},{"name":"ETC","description":"Ethereum Classic"},{"name":"BCH","description":"Bitcoin Cash"},{"name":"PLN","description":"Polish z\u0142oty"},{"name":"BTG","description":"Bitcoin Gold"},{"name":"EOS","description":"EOS"},{"name":"XLM","description":"Stellar"},{"name":"OMG","description":"OmiseGO"},{"name":"TRX","description":"TRON"},{"name":"ADA","description":"Cardano"},{"name":"NEO","description":"NEO"},{"name":"GAS","description":"Gas"},{"name":"ZRX","description":"0x"},{"name":"GUSD","description":"Gemini Dollar"},{"name":"XEM","description":"NEM"},{"name":"SMART","description":"SmartCash"},{"name":"QTUM","description":"Qtum"},{"name":"HB","description":"HeartBout"},{"name":"DAI","description":"Dai"},{"name":"MKR","description":"Maker"},{"name":"MNC","description":"Maincoin"},{"name":"USDC","description":"USD Coin"},{"name":"ROOBEE","description":"ROOBEE"},{"name":"DCR","description":"Decred"},{"name":"XTZ","description":"Tezos"},{"name":"EXM","description":"EXMO coin"},{"name":"ZAG","description":"Zag"},{"name":"BTT","description":"BitTorrent"},{"name":"HP","description":"HeartBout PAY"},{"name":"KZT","description":"Kazakhstan Tenge"},{"name":"CRON","description":"Cryptocean"},{"name":"ONT","description":"Ontology"},{"name":"ONG","description":"Ontology Gas"},{"name":"ALGO","description":"Algorand"},{"name":"GBP","description":"Pound sterling"},{"name":"ATOM","description":"Cosmos"},{"name":"WXT","description":"Wirex token"},{"name":"CHZ","description":"Chiliz"},{"name":"ONE","description":"Harmony"},{"name":"IQN","description":"IQeon"},{"name":"PRQ","description":"PARSIQ"},{"name":"HAI","description":"Hacken Token"},{"name":"LINK","description":"Chainlink"},{"name":"UNI","description":"Uniswap"},{"name":"YFI","description":"yearn.finance"},{"name":"GNY","description":"GNY"},{"name":"XYM","description":"Symbol"},{"name":"VITAE","description":"Vitae"},{"name":"BTCV","description":"Bitcoin Vault"},{"name":"DOT","description":"Polkadot"},{"name":"TONCOIN","description":"TON Coin"}]

def search_value(param,value,js=js):
    res=[]
    for i in js:
        if param in i.keys():
            if value.upper() in i[param].upper():
                res.append(i)
    return res
                

@app.route('/<path:url>', methods=['DELETE','POST','GET'])
def root(url):
    if 'Content-Type' in request.headers.keys():
        if request.headers['Content-Type']=='json/application':
            if request.method == 'POST':
                if url == 'search':
                    if request.args != None:
                        res=search_value(request.args['param'],request.args['value'])
                        return {'data':res}
    
    
    return '',404

